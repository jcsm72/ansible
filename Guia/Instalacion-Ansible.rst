Instalacion de Ansible - Centos 7
================================

Para obtener Ansible para CentOS 7, primero asegúrese de que el repositorio EPEL de CentOS 7 esté instalado: ::

    sudo yum install epel-release

Una vez que el repositorio esté instalado, instale Ansible con yum ::

    sudo yum install ansible

Ahora tenemos todo el software necesario para administrar nuestros servidores a través de Ansible 

configuración de hosts Ansible
===============================

Ansible realiza un seguimiento de todos los servidores que conoce a través de un archivo "hosts". Primero debemos configurar este archivo antes de que podamos comenzar a comunicarnos con nuestros servidores

Abra el archivo con privilegios de root como este: ::


    sudo vi /etc/ansible/hosts

Verá un archivo que tiene muchas configuraciones de ejemplo comentadas. Guarde estos ejemplos en el archivo para ayudarlo a conocer la configuración de Ansible si desea implementar escenarios más complejos.

la sintaxis que vamos a utilizar se parece a esto: ::

    [group_name]
    alias ansible_ssh_host=your_server_ip
El group_namees una etiqueta de organización que le permite referirse a cualquier servidores que aparecen debajo de ella con una palabra. El alias es solo un nombre para referirse a ese servidor.

Vamos a suponer que las direcciones IP de nuestros servidores son 192.0.2.1, 192.0.2.2y 192.0.2.3. Queremos poder hacer referencia a estos de forma individual como host1, host2y host3, o como un grupo como servers.  Para configurar esto, agregaría este bloque a su archivo de hosts: ::\

    [servers]
    host1 ansible_ssh_host=192.0.2.1
    host2 ansible_ssh_host=192.0.2.2
    host3 ansible_ssh_host=192.0.2.3

Ansible, de forma predeterminada, intentará conectarse a hosts remotos utilizando su nombre de usuario actual. Si ese usuario no existe en el sistema remoto, un intento de conexión resultará en este error: ::

    Ansible connection error
    host1 | UNREACHABLE! => {
        "changed": false,
        "msg": "Failed to connect to the host via ssh.",
        "unreachable": true
    }

Digamos específicamente a Ansible que debe conectarse a los servidores del grupo servers con el usuario sammy.  Cree un directorio en la estructura de configuración de Ansible llamado group_vars. ::

    sudo mkdir /etc/ansible/group_vars

Dentro de esta carpeta, podemos crear archivos con formato YAML para cada grupo que queramos configurar: ::

    sudo vim /etc/ansible/group_vars/servers

Agregue este código al archivo: ::

    ---
    ansible_ssh_user: sammy

Los archivos YAML comienzan con "-", así que asegúrese de no olvidar esa parte.

Ahora Ansible siempre usará el usuario sammy para el serversgrupo, independientemente del usuario actual.

Configura conexiones SSH sin password
=====================================

En el servidor donde esta corriendo el Ansible ejecutar el siguiente comando :: 

    ssh-keygen -b 4096 -t rsa

Esto generará una llave pública. 

Cuando pongan ese comando, les aparecerá eso: ::

    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/mobaxterm/.ssh/id_rsa):

Simplemente presionemos [Enter], un segundo después volvemos a presionar [Enter], y un segundo después volvemos una vez más a presionar [Enter]. O sea, presionaríamos [Enter] un total de tres (3) veces, solo lo presionamos… no escribimos nada

Listo, ya tenemos la llave pública… ahora falta dársela a quien queramos conectar

Ejecutamos lo siguiente: ::

    ssh-copy-id sammy@192.0.2.1

Esto lo que hace es simplemente darle la llave pública del servidor de Ansible al que nos queremos conectar 

Uso de comandos simples de Ansible

Ahora que tenemos nuestros hosts configurados y suficientes detalles de configuración para permitirnos conectarnos con éxito a nuestros hosts, podemos probar nuestro primer comando.

Haga ping a todos los servidores que configuró escribiendo: ::

    ansible -m ping all

Ansible devolverá un resultado como este: ::

    Output
    host1 | SUCCESS => {
        "changed": false,
        "ping": "pong"
    }

    host3 | SUCCESS => {
        "changed": false,
        "ping": "pong"
    }

    host2 | SUCCESS => {
        "changed": false,
        "ping": "pong"
    }

Esta es una prueba básica para asegurarse de que Ansible tenga una conexión con todos sus hosts.

También puede especificar un host individual: ::

    ansible -m ping host1

Puede especificar varios hosts separándolos con dos puntos: ::

    ansible -m ping host1:host2

El shellmódulo nos permite enviar un comando de terminal al host remoto y recuperar los resultados. Por ejemplo, para averiguar el uso de memoria en nuestra máquina host1, podríamos usar: ::

    ansible -m shell -a 'free -m' host1

Como puede ver, pasa argumentos a un script usando el -ainterruptor. Así es como podría verse la salida: ::

    Output
    host1 | SUCCESS | rc=0 >>
                 total       used       free     shared    buffers     cached
    Mem:          3954        227       3726          0         14         93
    -/+ buffers/cache:        119       3834
    Swap:            0          0          0


.. _Ansible: https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-centos-7
.. _SSH_NO_PASS : https://blog.desdelinux.net/ssh-sin-password-solo-3-pasos/